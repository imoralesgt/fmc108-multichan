/*
 * adc.h
 *
 *  Created on: Mar 8, 2023
 *      Author: ivan
 */

#ifndef SRC_ADC_H_
#define SRC_ADC_H_

int adc_init(int dma_device_id);
void adc_reset();
int adc_get_frame(int* buf, int samples);


void coincidenceSetupDefault(u32 CB_BASEADDR);

#endif /* SRC_ADC_H_ */
