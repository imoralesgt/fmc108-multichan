/*
 * spiPeripheral.h
 *
 *  Created on: Apr 1, 2022
 *      Author: ivan
 */

#ifndef SRC_SPIPERIPHERAL_H_
#define SRC_SPIPERIPHERAL_H_

#include "xparameters.h"
#include "xspips.h"
#include "xil_printf.h"


// =========================================================================

static XSpiPs SpiInstance;	 /* The instance of the SPI device */
#define SPI_DEVICE_ID		XPAR_XSPIPS_0_DEVICE_ID

// AD9510-related function prototypes
int ad9510Init(XSpiPs *SpiInstancePtr, u16 SpiDeviceId);
int ad9510SendCommand(XSpiPs *SpiInstancePtr, u8 spiRegister, u8 spiValue);

/*
 * Choose only one value of sampling frequency
 * Assign 1 to the selected value
 */
#define SAMPLING_FREQUENCY_250M		0
#define SAMPLING_FREQUENCY_210M		0
#define SAMPLING_FREQUENCY_180M		0
#define SAMPLING_FREQUENCY_156M		1  // 156.25 Msps
#define SAMPLING_FREQUENCY_139M		0  // 138.88 Msps
#define SAMPLING_FREQUENCY_125M		0
#define SAMPLING_FREQUENCY_97M		0
#define SAMPLING_FREQUENCY_90M		0
#define SAMPLING_FREQUENCY_75M		0

// Preprocessor for divider to choose sampling rate (in MHz)
#if SAMPLING_FREQUENCY_250M
	#define AD9510_DIVIDER_VAL_LOW	0x02 << 4
	#define AD9510_DIVIDER_VAL_HIGH	0x01
#elif SAMPLING_FREQUENCY_210M
	#define AD9510_DIVIDER_VAL_LOW	0x02 << 4
	#define AD9510_DIVIDER_VAL_HIGH	0x02
#elif SAMPLING_FREQUENCY_180M
	#define AD9510_DIVIDER_VAL_LOW	0x03 << 4
	#define AD9510_DIVIDER_VAL_HIGH	0x02
#elif SAMPLING_FREQUENCY_156M
	#define AD9510_DIVIDER_VAL_LOW	0x03 << 4
	#define AD9510_DIVIDER_VAL_HIGH	0x03
#elif SAMPLING_FREQUENCY_139M
	#define AD9510_DIVIDER_VAL_LOW	0x04 << 4
	#define AD9510_DIVIDER_VAL_HIGH	0x03
#elif SAMPLING_FREQUENCY_125M
	#define AD9510_DIVIDER_VAL_LOW	0x04 << 4
	#define AD9510_DIVIDER_VAL_HIGH	0x04
#elif SAMPLING_FREQUENCY_97M
	#define AD9510_DIVIDER_VAL_LOW	0x05 << 4
	#define AD9510_DIVIDER_VAL_HIGH	0x06
#elif SAMPLING_FREQUENCY_90M
	#define AD9510_DIVIDER_VAL_LOW	0x06 << 4
	#define AD9510_DIVIDER_VAL_HIGH	0x06
#elif SAMPLING_FREQUENCY_75M
	#define AD9510_DIVIDER_VAL_LOW	0x08 << 4
	#define AD9510_DIVIDER_VAL_HIGH	0x07
#endif

// AD9510-related registers
#define AD9510_HEADER_WRITE	0x00 //Write command, W0 = 0, W1 = 0 (Write 1 byte on each address)

// Address map
#define AD9510_REG_INSTRUCTION_HEADER	0x00 //Used as the MSB 0x00 instruction header in 1-byte (W1=W0=0b)
#define AD9510_REG_CLOCK_SELECT	0x45
#define AD9510_REG_PLL_CONTROL	0x0A //Used for PLL wake-up and Prescaler
#define AD9510_REG_A_COUNTER	0x04
#define AD9510_REG_B_COUNTER_LSB	0x06
#define AD9510_REG_R_DIVIDER_LSB	0x0C
#define AD9510_REG_CHARGEPMP_UP	0x08
#define AD9510_REG_PLLMUX_STATUS	AD9510_REG_CHARGEPMP_UP
#define AD9510_REG_CHARGEPMP_CURRENT 0x09
#define AD9510_REG_DIVIDER_CH0_VAL	0x48 //Clock distribution divider CH0
#define AD9510_REG_DIVIDER_CH1_VAL	0x4A //Clock distribution divider CH1
#define AD9510_REG_DIVIDER_CH2_VAL	0x4C //Clock distribution divider CH2
#define AD9510_REG_DIVIDER_CH3_VAL	0x4E //Clock distribution divider CH3
#define AD9510_REG_DIVIDER_CH4_VAL	0x50 //Clock distribution divider CH4
#define AD9510_REG_DIVIDER_CH5_VAL	0x52 //Clock distribution divider CH5
#define AD9510_REG_DIVIDER_CH6_VAL	0x54 //Clock distribution divider CH6
#define AD9510_REG_DIVIDER_CH7_VAL	0x56 //Clock distribution divider CH7
#define AD9510_REG_UPDATEREGS	0x5A //Write '1' after any register has been modified

// Default register values for init. sequence
#define AD9510_CLK1_OFF		0x01 << 1 //CLK1 power-down
#define AD9510_CLKSEL_CLK_2	0x00 //Choose CLK2 as PLL source (coming from VCO output)
#define AD9510_INIT_CLK_2	AD9510_CLK1_OFF | AD9510_CLKSEL_CLK_2
#define AD9510_PLL_WAKEUP	0x00 //PLL power on to normal operation
#define AD9510_PRESCALER	0x04 << 2 //Prescaler to 8 DM
#define AD9510_INIT_PLL_CONTROL	AD9510_PLL_WAKEUP | AD9510_PRESCALER
#define AD9510_INIT_A_COUNTER	0x06 //Setting A counter to 6 (datasheet recommendation was 0x05, though)
#define AD9510_INIT_B_COUNTER	0x0F //Setting B counter to 15
#define AD9510_INIT_R_DIVIDER	0x0A //Setting R-divider to 10
#define AD9510_CHARGEPMP_NORMAL	0x03	//Turning on charge pump in normal operation range
#define AD9510_CHARGEPMP_PMP_DN	0x02	//Turning on charge pump in higher operation range
#define AD9510_CHARGEPMP_PMP_UP	0x01	//Turning on charge pump in lower operation range
#define AD9510_STATUS_TRISTATE	0x0B << 2 //Status output to tri-state mode (REF_EN enable)
#define AD9510_PFD_POLARITY_POSITIVE	0x01 << 6
#define AD9510_PFD_POLARITY_NEGATIVE	0x00 << 6
#define AD9510_INIT_PLLMUX	AD9510_STATUS_TRISTATE | AD9510_CHARGEPMP_NORMAL | AD9510_PFD_POLARITY_POSITIVE
#define AD9510_INIT_CHARGEPMP_I 0x07 << 4 //Setting charge pump current to 4.8 mA
#define AD9510_DIVIDER_INIT_VALUE	AD9510_DIVIDER_VAL_LOW | AD9510_DIVIDER_VAL_HIGH
#define AD9510_UPDATEREGS_BIT	0x01



#endif /* SRC_SPIPERIPHERAL_H_ */
