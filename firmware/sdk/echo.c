/*
 * Copyright (C) 2016 - 2019 Xilinx, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <string.h>

#include "lwip/sockets.h"
#include "netif/xadapter.h"
#include "lwipopts.h"
#include "xil_printf.h"
#include "FreeRTOS.h"
#include "task.h"
#include "xparameters.h"
#include "comblock.h"
#include "spiPeripheral.h"
#include "common.h"
#include "adc.h"

#define THREAD_STACKSIZE 1024*2

u16_t echo_port = 10000;

void print_echo_app_header()
{
    xil_printf("%20s %6d %s\r\n", "echo server",
                        echo_port,
                        "$ telnet <board_ip> 7");

}

/* thread spawned for each connection */
void process_echo_request(void *p)
{
	int sd = (int)p;
	int buf_rx[5];
	int n;
	int *buf_tx;
	int test;
	int status;
	int samples;
	int bitslip;
	int threshold;

	buf_tx = (int *) MEM_BASE_ADDR_CHAN_A;

		/* read a max of packet size bytes from socket */
	if ((n = read(sd, buf_rx, 4*4)) < 0) {
		xil_printf("%s: error reading from socket %d, closing socket\r\n", __FUNCTION__, sd);
	}
	adc_reset();
//	xil_printf("ADC successfully initialized!\n\r");

	samples = buf_rx[0];
	test    = buf_rx[1];
	bitslip = buf_rx[2];
	threshold = buf_rx[3];


//	xil_printf("\n Configurations received \n\r");
//	xil_printf("Samples: %i - Test: %i  - Bitslip: %i  - Ch: %i \n \r",samples,test,bitslip,threshold);





	// Xil_Out32(XPAR_REGS_TO_AXIL_0_AXIL_BASEADDR+TEST_ADDR,test);
	// Xil_Out32(XPAR_REGS_TO_AXIL_0_AXIL_BASEADDR+SAMPLE_ADDR,samples);
	// Xil_Out32(XPAR_REGS_TO_AXIL_0_AXIL_BASEADDR+CHAN_ADDR,channel);
	//
//	cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_TEST, test);
//	cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_SAMPLES, samples);
//	cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_CHANNEL, channel);

	threshold = 0;

	cbWriteReg(COMBLOCK_0, CBREG_NUM_SAMPLES, BITS_N_SAMPLES, samples << OFFSET_N_SAMPLES);
	cbWriteReg(COMBLOCK_0, CBREG_CHANNEL_A_CONFIG, BITS_CHAN_TEST, (test) << OFFSET_CHAN_TEST);
	cbWriteReg(COMBLOCK_0, CBREG_CHANNEL_A_CONFIG, BITS_CHAN_THRESHOLD, (((unsigned int)(signed int)(threshold)) << OFFSET_CHAN_THRESHOLD));

	xil_printf("Threshold set to: %i \n\r", (signed int)(threshold));


	status = adc_get_frame(buf_tx, samples);

	if (status == XST_SUCCESS){
		if ((n = write(sd, buf_tx, samples * sizeof(int))) < 0) {
			xil_printf("ERROR responding to client echo request. Closing socket %d\n");
		}
	}

	//i2cTest();
	//SpiRunExample();

	/* close connection */
	close(sd);
	vTaskDelete(NULL);
}

void echo_application_thread()
{
	int sock, new_sd;
	int size;
#if LWIP_IPV6==0
	struct sockaddr_in address, remote;

	memset(&address, 0, sizeof(address));

	if ((sock = lwip_socket(AF_INET, SOCK_STREAM, 0)) < 0)
		return;

	address.sin_family = AF_INET;
	address.sin_port = htons(echo_port);
	address.sin_addr.s_addr = INADDR_ANY;
#else
	struct sockaddr_in6 address, remote;

	memset(&address, 0, sizeof(address));

	address.sin6_len = sizeof(address);
	address.sin6_family = AF_INET6;
	address.sin6_port = htons(echo_port);

	memset(&(address.sin6_addr), 0, sizeof(address.sin6_addr));

	if ((sock = lwip_socket(AF_INET6, SOCK_STREAM, 0)) < 0)
		return;
#endif

	if (lwip_bind(sock, (struct sockaddr *)&address, sizeof (address)) < 0)
		return;

	lwip_listen(sock, 0);

	size = sizeof(remote);

	while (1) {
		if ((new_sd = lwip_accept(sock, (struct sockaddr *)&remote, (socklen_t *)&size)) > 0) {
			sys_thread_new("echos", process_echo_request,
				(void*)new_sd,
				THREAD_STACKSIZE,
				DEFAULT_THREAD_PRIO);
		}
	}
}
