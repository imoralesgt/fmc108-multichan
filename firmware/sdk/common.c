/*
 * common.c
 *
 *  Created on: Mar 8, 2023
 *      Author: ivan
 */


#include "common.h"

// Set (assert) all the bits within the CB_BITS in the CB_REG
void cbSetBits(u32 cbBaseAddr, u32 cbReg, u32 cbBits){
	u32 currentRegValue;
	currentRegValue = cbRead(cbBaseAddr, cbReg);
	currentRegValue |= cbBits;
	cbWrite(cbBaseAddr, cbReg, currentRegValue);
}

// Reset (deassert) all the bits within the CB_BITS in the CB_REG
void cbResetBits(u32 cbBaseAddr, u32 cbReg, u32 cbBits){
	u32 currentRegValue;
	currentRegValue = cbRead(cbBaseAddr, cbReg);
	currentRegValue &= ~(cbBits);
	cbWrite(cbBaseAddr, cbReg, currentRegValue);
}

// Write a value to a CB Register without overwriting current status
void cbWriteReg(u32 cbBaseAddr, u32 cbReg, u32 registerMask, u32 newValue){
	u32 currentRegValue;
	currentRegValue = cbRead(cbBaseAddr, cbReg);
	currentRegValue = (currentRegValue & ~registerMask) | (newValue & registerMask);
	cbWrite(cbBaseAddr, cbReg, currentRegValue);
}
