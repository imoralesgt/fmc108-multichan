
//---AXI TO REGISTER MAP ADDRESS----
//#define BITSLIP_ADDR 0x00
//#define START_ADDR   BITSLIP_ADDR + 0x04
//#define SAMPLE_ADDR  START_ADDR + 0x04
//#define TEST_ADDR    SAMPLE_ADDR + 0x04
//#define CHAN_ADDR    TEST_ADDR + 0x04 + 0x04

#include "comblock.h"
#include "xparameters.h"

#define CB_START CB_OREG0
#define CB_SAMPLES CB_OREG1
#define CB_TEST CB_OREG2
#define CB_CHANNEL CB_OREG3

#define CHAN_ADDR 0x00
#define START_ADDR   CHAN_ADDR + 0x04
#define SAMPLE_ADDR  START_ADDR + 0x04
#define TEST_ADDR    SAMPLE_ADDR + 0x04
#define BITSLIP_ADDR    TEST_ADDR + 0x04


//#define MEM_BASE_ADDR 0x10000000
//#define MEM_BASE_ADDR_CHAN_A	XPAR_PS7_RAM_0_S_AXI_BASEADDR + 0x100000
#define MEM_BASE_ADDR_CHAN_A	0x10000000
#define MEM_BASE_ADDR_CHAN_G	XPAR_PS7_RAM_1_S_AXI_BASEADDR + 0x100000



/* ====================
 * ComBlock definitions
 * ====================
 */
#define COMBLOCK_0	XPAR_COMBLOCK_0_AXIL_BASEADDR



/* ==============================
 * ComBlock-related registers map
 * ==============================
 */
#define CBREG_CHANNEL_A_CONFIG	CB_OREG0
#define CBREG_CHANNEL_G_CONFIG	CB_OREG1
#define CBREG_NUM_SAMPLES		CB_OREG2
#define CBREG_COINCIDENCE_SETUP	CB_OREG3



/* ============================
 * Channel config register bits
 * ============================
 */

// Masks
#define BITS_CHAN_THRESHOLD		0x0000FFFF
#define BITS_CHAN_SBT			0x03FF0000
#define BITS_CHAN_EDGE			0x04000000
#define BITS_CHAN_ENABLE		0x08000000
#define BITS_CHAN_TEST			0x10000000

// Bit shift offsets
#define OFFSET_CHAN_THRESHOLD	0x00000000
#define OFFSET_CHAN_SBT			0x00000010
#define OFFSET_CHAN_EDGE		0x0000001A
#define OFFSET_CHAN_ENABLE		0x0000001B
#define OFFSET_CHAN_TEST		0x0000001C


/* ===============================
 * Coincidence logic register bits
 * ===============================
 */

#define CHANNEL_A	0
#define CHANNEL_G	1

// Masks
#define BITS_COINC_WINDOW_L		0x00000FFF
#define BITS_COINC_FIRST_CHAN	0x00003000
#define BITS_COINC_ORDER		0x00004000
#define BITS_COINC_MODE			0x00008000

// Bit shift offsets
#define OFFSET_COINC_WINDOW_L	0x00000000
#define OFFSET_COINC_FIRST_CHAN	0x0000000C
#define OFFSET_COINC_ORDER		0x0000000E
#define OFFSET_COINC_MODE		0x0000000F



/* ===============================
 * Number of samples register bits
 * ===============================
 */

// Masks
#define BITS_N_SAMPLES			0xFFFFFFFF

//Bit shift offsets
#define OFFSET_N_SAMPLES		0x00000000


/*
 * Function prototypes
 */
void cbSetBits(u32 cbBaseAddr, u32 cbReg, u32 cbBits);
void cbResetBits(u32 cbBaseAddr, u32 cbReg, u32 cbBits);
void cbWriteReg(u32 cbBaseAddr, u32 cbReg, u32 registerMask, u32 newValue);



