/***************************** Include Files *********************************/

#include "xparameters.h"	/* SDK generated parameters */
#include "xplatform_info.h"
#include "xspips.h"		/* SPI device driver */
#include "xil_printf.h"
#include "spiPeripheral.h"
#include "sleep.h"

/************************** Constant Definitions *****************************/


/*
 * =======================
 * Custom AD9510 functions
 * =======================
 */

int ad9510Init(XSpiPs *SpiInstancePtr, u16 SpiDeviceId);
int ad9510SendCommand(XSpiPs *SpiInstancePtr, u8 spiRegister, u8 spiValue);


//Send a single command using AD9510 HEADER+REGISTER+VALUE SPI format
int ad9510SendCommand(XSpiPs *SpiInstancePtr, u8 spiRegister, u8 spiValue){
	u8 commandBuffer[3];
	int status;

	commandBuffer[0] = AD9510_REG_INSTRUCTION_HEADER;
	commandBuffer[1] = spiRegister;
	commandBuffer[2] = spiValue;

	status = XSpiPs_PolledTransfer(SpiInstancePtr, commandBuffer, NULL, sizeof(commandBuffer));

	return status;
}

int ad9510Init(XSpiPs *SpiInstancePtr, u16 SpiDeviceId){

	XSpiPs_Config *SpiConfig;
	u8 initBuffer[3];
	int Status;


	/*
	 * Initialize the SPI driver so that it's ready to use
	 */
	SpiConfig = XSpiPs_LookupConfig(SpiDeviceId);
	if (NULL == SpiConfig) {
		return XST_FAILURE;
	}

	Status = XSpiPs_CfgInitialize(SpiInstancePtr, SpiConfig,
					SpiConfig->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Perform a self-test to check hardware build
	 */
	Status = XSpiPs_SelfTest(SpiInstancePtr);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Set the SPI device as a master with manual start and manual
	 * chip select mode options
	 */
	XSpiPs_SetOptions(SpiInstancePtr, XSPIPS_MANUAL_START_OPTION | \
			XSPIPS_MASTER_OPTION | XSPIPS_FORCE_SSELECT_OPTION);

	/*
	 * Set the SPI device pre-scalar to divide by 32 (5 MHz SCLK)
	 */
	XSpiPs_SetClkPrescaler(SpiInstancePtr, XSPIPS_CLK_PRESCALE_32);

	/*
	 * Set the SPI Chip=Select (Slave-select) number
	 */
	XSpiPs_SetSlaveSelect(SpiInstancePtr, 0);


	// Choose CLK2 as clock source for clock tree
	ad9510SendCommand(SpiInstancePtr,
			AD9510_REG_CLOCK_SELECT,
			AD9510_INIT_CLK_2);

	// PLL N-divider = (Prescaler*B + A)
	// PLL wake-up and Prescaler
	ad9510SendCommand(SpiInstancePtr,
			AD9510_REG_PLL_CONTROL,
			AD9510_INIT_PLL_CONTROL);

	// N divider (B counter)
	ad9510SendCommand(SpiInstancePtr,
			AD9510_REG_B_COUNTER_LSB,
			AD9510_INIT_B_COUNTER);

	// N divider (A counter)
	ad9510SendCommand(SpiInstancePtr,
			AD9510_REG_A_COUNTER,
			AD9510_INIT_A_COUNTER);

	// PFD R-divider set to 10
	ad9510SendCommand(SpiInstancePtr,
			AD9510_REG_R_DIVIDER_LSB,
			AD9510_INIT_R_DIVIDER);

	// Charge pump current set to 4.8 mA
	ad9510SendCommand(SpiInstancePtr,
			AD9510_REG_CHARGEPMP_CURRENT,
			AD9510_INIT_CHARGEPMP_I);

	// Charge pump enable and Status to Tri-state (Internal reference enable)
	ad9510SendCommand(SpiInstancePtr,
			AD9510_REG_PLLMUX_STATUS,
			AD9510_INIT_PLLMUX);

	//Setting up CH0 output clock divider to 5 (1250 MHz -> 250 MHz)
	ad9510SendCommand(SpiInstancePtr,
			AD9510_REG_DIVIDER_CH0_VAL,
			AD9510_DIVIDER_INIT_VALUE);

	//Setting up CH1 output clock divider to 5 (1250 MHz -> 250 MHz)
	ad9510SendCommand(SpiInstancePtr,
			AD9510_REG_DIVIDER_CH1_VAL,
			AD9510_DIVIDER_INIT_VALUE);

	//Setting up CH2 output clock divider to 5 (1250 MHz -> 250 MHz)
	ad9510SendCommand(SpiInstancePtr,
			AD9510_REG_DIVIDER_CH2_VAL,
			AD9510_DIVIDER_INIT_VALUE);

	//Setting up CH3 output clock divider to 5 (1250 MHz -> 250 MHz)
	ad9510SendCommand(SpiInstancePtr,
			AD9510_REG_DIVIDER_CH3_VAL,
			AD9510_DIVIDER_INIT_VALUE);

	//Setting up CH4 output clock divider to 5 (1250 MHz -> 250 MHz)
	ad9510SendCommand(SpiInstancePtr,
			AD9510_REG_DIVIDER_CH4_VAL,
			AD9510_DIVIDER_INIT_VALUE);

	//Setting up CH5 output clock divider to 5 (1250 MHz -> 250 MHz)
	ad9510SendCommand(SpiInstancePtr,
			AD9510_REG_DIVIDER_CH5_VAL,
			AD9510_DIVIDER_INIT_VALUE);

	//Setting up CH6 output clock divider to 5 (1250 MHz -> 250 MHz)
	ad9510SendCommand(SpiInstancePtr,
			AD9510_REG_DIVIDER_CH6_VAL,
			AD9510_DIVIDER_INIT_VALUE);

	//Setting up CH7 output clock divider to 5 (1250 MHz -> 250 MHz)
	ad9510SendCommand(SpiInstancePtr,
			AD9510_REG_DIVIDER_CH7_VAL,
			AD9510_DIVIDER_INIT_VALUE);

	// Update registers
	ad9510SendCommand(SpiInstancePtr,
			AD9510_REG_UPDATEREGS,
			AD9510_UPDATEREGS_BIT);

	return XST_SUCCESS;

}

