#include "xaxidma.h"
#include "common.h"
#include "comblock.h"
#include "spiPeripheral.h"
#include "adc.h"

static XAxiDma dma_inst;

int adc_init(int dma_device_id) {
	int				status;
	XAxiDma_Config*	axicfg;

	// Init clocking tree (AD9510) custom setup (250 MHz clock to ADC)
	status = ad9510Init(&SpiInstance, XPAR_XSPIPS_0_DEVICE_ID);


	// Init coincidence block to single trigger mode on Channel A (default configuration)
	coincidenceSetupDefault(COMBLOCK_0);

	xil_printf("Clock tree configured to 250 MSPS\n\r");
	if (status != XST_SUCCESS){
		xil_printf("Error configuring SPI Clock Tree, failed with error code %d. \r\n", status);
		return XST_FAILURE;
	}

	// DMA Config -------------------------------------------------------------
	axicfg = XAxiDma_LookupConfig(dma_device_id);
	if (!axicfg) {
		xil_printf("ERROR! No hardware configuration found for AXI DMA with device id %d.\r\n", dma_device_id);
		return XST_FAILURE;
	}
	status = XAxiDma_CfgInitialize(&dma_inst, axicfg);
	if (status != XST_SUCCESS) {
		xil_printf("ERROR! Initialization of AXI DMA failed with %d\r\n", status);
		return XST_FAILURE;
	}
	// Workaround
	XAxiDma_Reset(&dma_inst);
	while (!XAxiDma_ResetIsDone(&dma_inst)) {}

	xil_printf("DMA initialized successfully\n\r");

	XAxiDma_IntrEnable(&dma_inst, (XAXIDMA_IRQ_IOC_MASK | XAXIDMA_IRQ_ERROR_MASK), XAXIDMA_DEVICE_TO_DMA);

	(void) XAxiDma_SimpleTransfer(&dma_inst, (unsigned int *) MEM_BASE_ADDR_CHAN_A, sizeof(int), XAXIDMA_DEVICE_TO_DMA);

	//
	return XST_SUCCESS;
}

void adc_reset() {
	cbResetBits(COMBLOCK_0, CBREG_CHANNEL_A_CONFIG, BITS_CHAN_ENABLE); //Disable acquisition on Channel A
	cbResetBits(COMBLOCK_0, CBREG_CHANNEL_G_CONFIG, BITS_CHAN_ENABLE); //Disable acquisition on Channel G
	cbSetBits(COMBLOCK_0, CBREG_CHANNEL_A_CONFIG, BITS_CHAN_EDGE); //Set falling edge on Channel A

	// How many samples will be acquired in total
	cbWriteReg(COMBLOCK_0, CBREG_CHANNEL_A_CONFIG, BITS_CHAN_THRESHOLD, ((unsigned int)(signed short) 50) << OFFSET_CHAN_THRESHOLD);

	// How many samples must be recorded before trigger
	cbWriteReg(COMBLOCK_0, CBREG_CHANNEL_A_CONFIG, BITS_CHAN_SBT, ((unsigned int)(signed short) 128) << OFFSET_CHAN_SBT);

	XAxiDma_Reset(&dma_inst);
	while (!XAxiDma_ResetIsDone(&dma_inst)) {}
}

int adc_get_frame(int* buf, int samples) {
	int status;
    Xil_DCacheFlushRange((int) buf, samples * sizeof(int));

    //Enable ADC
	cbSetBits(COMBLOCK_0, CBREG_CHANNEL_A_CONFIG, BITS_CHAN_ENABLE);

	// Kick off S2MM transfer
	status = XAxiDma_SimpleTransfer(&dma_inst, (int) buf, samples * sizeof(int), XAXIDMA_DEVICE_TO_DMA);
	if (status != XST_SUCCESS) {
		xil_printf("ERROR! Failed to kick off S2MM transfer!\n\r");
		return XST_FAILURE;
	}

	// Wait for transfer to complete
	while (XAxiDma_Busy(&dma_inst, XAXIDMA_DEVICE_TO_DMA))
		;

	//Disable ADC
	cbResetBits(COMBLOCK_0, CBREG_CHANNEL_A_CONFIG, BITS_CHAN_ENABLE);

	if ((XAxiDma_ReadReg(dma_inst.RegBase, XAXIDMA_RX_OFFSET+XAXIDMA_SR_OFFSET) & XAXIDMA_IRQ_ERROR_MASK) != 0) {
		xil_printf("ERROR! AXI DMA returned an error during the S2MM transfer.\n\r");
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}


void coincidenceSetupDefault(u32 CB_BASEADDR){
	cbWriteReg(COMBLOCK_0, CBREG_COINCIDENCE_SETUP, BITS_COINC_WINDOW_L, (10) << OFFSET_COINC_WINDOW_L); //Coincidence window
	cbWriteReg(COMBLOCK_0, CBREG_COINCIDENCE_SETUP, BITS_COINC_FIRST_CHAN, (CHANNEL_A) << OFFSET_COINC_FIRST_CHAN); //First channel to trigger the event
	cbWriteReg(COMBLOCK_0, CBREG_COINCIDENCE_SETUP, BITS_COINC_ORDER, (0) << OFFSET_COINC_ORDER); //Does trigger order matter? Yes!
	cbWriteReg(COMBLOCK_0, CBREG_COINCIDENCE_SETUP, BITS_COINC_MODE, (0) << OFFSET_COINC_MODE); //Single trigger mode, no coincidence
}

