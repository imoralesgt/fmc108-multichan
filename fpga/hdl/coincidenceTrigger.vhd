----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/03/2023 01:44:44 PM
-- Design Name: 
-- Module Name: coincidenceTrigger - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Receives an array of trigger events and
-- 				generates a single output trigger signal according
--				to the requested event detection setup
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;


entity coincidenceTrigger is
    Generic(
        TRIGGER_SOURCE_COUNT : NATURAL := 2;
        TRIG_WINDOW_BUSWIDTH : NATURAL := 12
    );
    Port (  clk : in STD_LOGIC;
            aresetn : in STD_LOGIC;

            -- Trigger source signal inputs
            triggerInBus : in STD_LOGIC_VECTOR(TRIGGER_SOURCE_COUNT - 1 downto 0);

            -- Coincidence output signal
            coincidenceOut : out STD_LOGIC;



            -- Output configuration register
            setupRegister : in STD_LOGIC_VECTOR(TRIGGER_SOURCE_COUNT + 1 + TRIG_WINDOW_BUSWIDTH downto 0)


            -- Coincidence setup register (coincidenceSetup) mapped as follows (N channels)
            -- | N+1 |  N  | N-1 ... 0 |    <--- Register bits
            -- | cmd | any |   FChan   |    <--- Register names


            
            -- N: number of channels for the coincidence lookup
            
            -- Register [cmd]:  '1' if MORE than 1 single event must trigger within time window (coincidence mode)
            --                  '0' if one trigger is enough to signal an event (single trigger mode)

            -- Register [Any]:  '1' if ANY channel is enabled to start the coincidence lookup
            --                  '0' will only enable the channel with a '1' bit in the FChan register to start the coincidence lookup

            -- Register [FChan]: Expects an array of bits to signal which is the channel capable of enabling the coincidence window start
            --                   ONLY one bit must be set to ensure correct functionality
            --                   [FChan] value is ignored if the [Any] register is set


            );
end coincidenceTrigger;

architecture Behavioral of coincidenceTrigger is
    signal rst : std_logic;
    signal windowCnt : unsigned(TRIG_WINDOW_BUSWIDTH - 1 downto 0);
    signal coincidenceSetupReg  : STD_LOGIC_VECTOR(TRIGGER_SOURCE_COUNT + 1 downto 0); 
    signal coincidenceWindowReg : STD_LOGIC_VECTOR(TRIG_WINDOW_BUSWIDTH - 1 downto 0); -- Clock cycles


    


    signal any : std_logic; -- Doesn't matter order of input trigger
    signal cmd : std_logic; -- Force TWO events to generate a trigger
    signal coincidenceChannelBits : std_logic_vector(TRIGGER_SOURCE_COUNT - 1 downto 0);
    signal coincidenceChannelBitsNot : std_logic_vector(TRIGGER_SOURCE_COUNT - 1 downto 0);

    signal thisCoincidence : std_logic_vector(TRIGGER_SOURCE_COUNT - 1 downto 0);
    signal thisCoincidenceNot : std_logic_vector(TRIGGER_SOURCE_COUNT - 1 downto 0);

    signal triggerCheck : std_logic_vector(TRIGGER_SOURCE_COUNT - 1 downto 0);
    signal coincidenceCheck : std_logic_vector(TRIGGER_SOURCE_COUNT - 1 downto 0);
    
    
    

    type fsmState is (IDLE, SCANNING, COINCIDENCE);
    signal currentState, nextState : fsmState;

    
begin

    rst <= not aresetn;

    -- Splitting out setup register into coincidence window length and coincidence mode registers
    coincidenceWindowReg <= setupRegister(TRIG_WINDOW_BUSWIDTH - 1 downto 0);
    coincidenceSetupReg  <= setupRegister(TRIGGER_SOURCE_COUNT + 1 + TRIG_WINDOW_BUSWIDTH downto TRIG_WINDOW_BUSWIDTH); 
    

    -- One-clock cycle tick to signal a coincidence event
    coincidenceOut <= '1' when currentState = COINCIDENCE else '0';

    cmd <= coincidenceSetupReg(TRIGGER_SOURCE_COUNT + 1);
    any <= coincidenceSetupReg(TRIGGER_SOURCE_COUNT);
    coincidenceChannelBits <= coincidenceSetupReg(TRIGGER_SOURCE_COUNT - 1 downto 0);
    coincidenceChannelBitsNot <= not coincidenceChannelBits;

    thisCoincidenceNot <= not thisCoincidence;

    triggerCheck <= coincidenceChannelBits and triggerInBus; -- First trigger detected
    coincidenceCheck <= thisCoincidenceNot and triggerInBus; -- Coincidence with a trigger different from the first trigger


    -- Event time window for coincidence counter
    process(clk)
    begin
        if (rising_edge(clk)) then
            if(rst = '1') then
                windowCnt <= (others => '0');
            else
                case (currentState) is                  
                    when SCANNING =>
                        windowCnt <= windowCnt + 1;
                    
                    when others =>
                        windowCnt <= (others => '0');

                end case;
            end if;
        end if;

    end process;

    
    -- FSM synchronous process
    process(clk)
    begin
        if (rising_edge(clk)) then
            if (rst = '1') then
                currentState <= IDLE;
            else
                currentState <= nextState;
            end if;
        end if;
    end process;

    -- FSM logic
    process(currentState, windowCnt)
    begin
        case (currentState) is
            when IDLE =>
                
                -- Register the current coincidence status before scan or create a coincidence event
                thisCoincidence <= coincidenceChannelBits;

                if(any = '1') then
                    -- Doesn't matter which channel started the coincidence
                    if(unsigned(coincidenceChannelBits) > 1) then
                        nextState <= SCANNING;
                    else
                        nextState <= currentState;
                    end if;

                else
                    -- If the expected channel started the trigger
                    if(unsigned(triggerCheck) > 1) then
                        -- If more than 1 event is expected
                        
                        if(cmd = '1') then
                            -- If more than 1 bit is on simultaneously, trigger immediately
                            if(unsigned(triggerInBus) > unsigned(coincidenceChannelBits)) then
                                nextState <= COINCIDENCE;
                            else
                                nextState <= SCANNING; -- Otherwise, wait for an event in the time window
                            end if;
                        else
                            nextState <= COINCIDENCE;
                        end if;
                    end if;
                end if;
            when SCANNING =>

                    -- If no events were detected within the window
                    if (windowCnt = unsigned(coincidenceWindowReg)) then
                        nextState <= IDLE;
                    else
                        -- If an event is detected from any channel other that that that started the scanning window
                        if (unsigned(coincidenceCheck) > 1) then
                            nextState <= COINCIDENCE;
                        else
                            nextState <= currentState;
                        end if;
                    end if;
            when COINCIDENCE =>
                
                -- FSM output logic will take care to generate the corresponding tick to signal the coincidence event
                nextState <= IDLE;

            when others =>
                nextState <= IDLE;
        end case;
    end process;

    


end Behavioral;