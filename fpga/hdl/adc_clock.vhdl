-- ADC Clock
-- Bruno Valinoti <valinoti at inti.gob.ar>
-- Rodrigo A. Melo <rmelo at inti.gob.ar>

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
library UNISIM;
use UNISIM.VCOMPONENTS.all;

entity adc_clock is
   generic (
      DIFF_TERM  : boolean:=TRUE;
      IOSTANDARD : string:="LVDS_25"
   );
   port (
      clk_p_i    :  in std_logic;
      clk_n_i    :  in std_logic;
      -- clk_div2_o : out std_logic;
      clk_o      : out std_logic
   );
end adc_clock;

architecture XILINX_Serie7 of adc_clock is
   signal clk        : std_logic;
begin
   ibufgds_inst : IBUFGDS
   generic map (DIFF_TERM => DIFF_TERM, IOSTANDARD => IOSTANDARD)
   port map (I => clk_p_i, IB => clk_n_i, O => clk);

   -- clkio_inst: BUFIO
   -- port map  (I => clk, O => clk_o);
  clk_o <= clk;
   -- clk_div2_inst : BUFR
   -- generic map (BUFR_DIVIDE => "2", SIM_DEVICE => "7SERIES")
   -- port map  (I => clk, O => clk_div2_o, CE => '1', CLR => '0');

end architecture XILINX_Serie7;
