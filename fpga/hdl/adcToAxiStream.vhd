----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/03/2023 05:13:24 PM
-- Design Name: 
-- Module Name: adcToAxiStream - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity adcToAxiStream is
    Generic(
        AXI_BUSWIDTH : NATURAL := 32;
        MAX_SAMPLES_BUSWIDTH : NATURAL := 32
    );
    Port (
          adcData : in STD_LOGIC_VECTOR (AXI_BUSWIDTH - 1 downto 0);
          trigIn : in STD_LOGIC;
          dValidIn : in STD_LOGIC;
          nSamples : in STD_LOGIC_VECTOR(MAX_SAMPLES_BUSWIDTH - 1 downto 0);
          enableIn    : in STD_LOGIC;
          M_AXIS_ACLK    :  in std_logic;
          M_AXIS_ARESETN :  in std_logic;
          M_AXIS_TVALID  : out std_logic;
          M_AXIS_TDATA   : out std_logic_vector(AXI_BUSWIDTH - 1 downto 0);
          M_AXIS_TSTRB   : out std_logic_vector(3 downto 0);
          M_AXIS_TLAST   : out std_logic;
          M_AXIS_TREADY  :  in std_logic
         );
end adcToAxiStream;

architecture Behavioral of adcToAxiStream is

    signal clk : std_logic;
    signal rst : std_logic;

    -- FSM-related control signals
    signal cnt : unsigned(31 downto 0);
    type   state_t is (IDLE, SEND);
    signal fsmState : state_t := IDLE;
    
    

begin

    clk <= M_AXIS_ACLK;
    rst <= not M_AXIS_ARESETN;
    
    -- AXIS interface
    M_AXIS_TDATA  <= adcData;
    M_AXIS_TVALID <= '1' when fsmState = SEND else '0';
    M_AXIS_TLAST  <= '1' when cnt = unsigned(nSamples) - 1 else '0';

    process(clk)
    begin
        if (rising_edge(clk)) then
            if(rst = '1') then
                cnt <= (others => '0');
                fsmState <= IDLE;               
            else
                case (fsmState) is
                    when IDLE =>
                        cnt <= (others => '0');
                        if (trigIn = '1' and enableIn = '1') then
                            fsmState <= SEND;
                        else
                            fsmState <= IDLE;
                        end if;
                    
                    when SEND =>
                        if (M_AXIS_TREADY = '1' and dValidIn = '1') then
                            cnt <= cnt + 1;
                            if (cnt = unsigned(nSamples) - 1) then
                                fsmState <= IDLE;
                            end if;
                        end if;
                end case;
            end if;
        end if;
    
    
    end process;








end Behavioral;
