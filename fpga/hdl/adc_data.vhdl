-- ADC DATA channel in DDR mode
-- Bruno Valinoti <valinoti at inti.gob.ar>
-- Rodrigo A. Melo <rmelo at inti.gob.ar>
--
-- DDR interface bit order for ADS62p49
-- DA0  = dat_in(0) ->  0  1
-- DA2  = dat_in(1) ->  2  3
-- DA4  = dat_in(2) ->  4  5
-- DA6  = dat_in(3) ->  6  7
-- DA8  = dat_in(4) ->  8  9
-- DA10 = dat_in(5) -> 10 11
-- DA12 = dat_in(6) -> 12 13


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
library UNISIM;
use UNISIM.VCOMPONENTS.all;
use STD.textio.all;
use ieee.std_logic_textio.all;

entity adc_data is
   generic (
      DIFF_TERM    : boolean:=TRUE;
      IOSTANDARD   : string:="LVDS_25";
      DAT_FILE_SRC : boolean:=FALSE
   );
   port (
      -- ctrl
      clk_i      :  in std_logic;
      -- clk_div2_i :  in std_logic;
      rst_i      :  in std_logic;
      ena_i      :  in std_logic;
      test_i     :  in std_logic;
      -- in
      dat_p_i    :  in std_logic_vector(6 downto 0);
      dat_n_i    :  in std_logic_vector(6 downto 0);
      -- out
      data_o     : out std_logic_vector(15 downto 0)
   );
end entity adc_data;

architecture XILINX_Serie7 of adc_data is
   signal dat_in        : std_logic_vector(6 downto 0);
   signal data_fake     : unsigned(15 downto 0):=(others => '0');
   signal data_adc      : std_logic_vector(15 downto 0):=(others => '0');
   -- signal data_aux      : std_logic_vector(15 downto 0):=(others => '0');
begin

   dat_gen : for i in 0 to 6 generate   --here generates the balanced to unbalanced data channel
      ibufds_inst : IBUFDS
      generic map (DIFF_TERM => FALSE, IOSTANDARD => "DEFAULT")
      port map (I => dat_p_i(i), IB => dat_n_i(i), O => dat_in(i));
   end generate dat_gen;

   -- See UG471 - CH. 2 - SelectIO Logic Resources (Input DDR Overview (IDDR))
  ch_gen : for i in 0 to 6 generate
     iddr_inst : IDDR   --IDDR_CLK2?
     generic map (
     DDR_CLK_EDGE => "OPPOSITE_EDGE", -- "OPPOSITE_EDGE", "SAME_EDGE" or "SAME_EDGE_PIPELINED"
     INIT_Q1 =>'0', -- Initial value of Q1: '0' or '1'
     INIT_Q2 =>'0', -- Initial value of Q2: '0' or '1'
     SRTYPE => "SYNC") -- Set/Reset type: "SYNC" or "ASYNC"
     port map (
     Q1 => data_adc(2*i+0), -- 1-bit output for positive edge of clock
     Q2 => data_adc(2*i+1), -- 1-bit output for negative edge of clock
     C =>  clk_i, -- 1-bit clock input
     CE => ena_i, -- 1-bit clock enable input
     D =>  dat_in(i), -- 1-bit DDR data input
     R => rst_i,-- 1-bit reset
     S => '0'
     );
     -- End of IDDR_inst instantiation
  end generate ch_gen;

  fake : process(clk_i)
  begin
     if rising_edge(clk_i) then
        if ena_i = '0' then
           data_fake <= (others => '0');
        else
           data_fake <= data_fake + 1;
        end if;
     end if;
  end process fake;
   -- data_aux <= "00"&data_adc(13)&"0000000000000";

  with  data_adc(13) select data_adc(15 downto 14) <=   "11" when '1', "00" when '0';
  data_o <= std_logic_vector(data_fake) when test_i = '1' else data_adc;

end architecture XILINX_Serie7;
