# ----------------------------------------------------------------------------
# User LEDs - Bank 33
# ----------------------------------------------------------------------------

## ----------------------------------------------------------------------------
## User DIP Switches - Bank 35
## ----------------------------------------------------------------------------
# "SW0"
# "SW1"
# "SW2"
# "SW3"
# "SW4"
# "SW5"
# "SW6"
# "SW7"
#set_property PACKAGE_PIN F22 [get_ports {dip_sw_i_0(0)}]
#set_property PACKAGE_PIN G22 [get_ports {dip_sw_i_0(1)}]
#set_property PACKAGE_PIN H22 [get_ports {dip_sw_i_0(2)}]
#set_property PACKAGE_PIN F21 [get_ports {dip_sw_i_0(3)}]
#set_property PACKAGE_PIN H19 [get_ports {dip_sw_i_0(4)}]
#set_property PACKAGE_PIN H18 [get_ports {dip_sw_i_0(5)}]
#set_property PACKAGE_PIN H17 [get_ports {dip_sw_i_0(6)}]
#set_property PACKAGE_PIN M15 [get_ports {dip_sw_i_0(7)}]
#set_property IOSTANDARD LVCMOS33 [get_ports {dip_sw_i_0(0)}]
#set_property IOSTANDARD LVCMOS33 [get_ports {dip_sw_i_0(1)}]
#set_property IOSTANDARD LVCMOS33 [get_ports {dip_sw_i_0(2)}]
#set_property IOSTANDARD LVCMOS33 [get_ports {dip_sw_i_0(3)}]
#set_property IOSTANDARD LVCMOS33 [get_ports {dip_sw_i_0(4)}]
#set_property IOSTANDARD LVCMOS33 [get_ports {dip_sw_i_0(5)}]
#set_property IOSTANDARD LVCMOS33 [get_ports {dip_sw_i_0(6)}]
#set_property IOSTANDARD LVCMOS33 [get_ports {dip_sw_i_0(7)}]



