## ----------------------------------------------------------------------------
## FMC Expansion Connector - Bank 13
## ----------------------------------------------------------------------------
#set_property PACKAGE_PIN R7 [get_ports {FMC_SCL}];  # "FMC-SCL"
#set_property PACKAGE_PIN U7 [get_ports {FMC_SDA}];  # "FMC-SDA"

## ----------------------------------------------------------------------------
## FMC Expansion Connector - Bank 33
## ----------------------------------------------------------------------------
#set_property PACKAGE_PIN AB14 [get_ports {FMC_PRSNT}];  # "FMC-PRSNT"

## ----------------------------------------------------------------------------
## FMC Connector to ADC channels
## ----------------------------------------------------------------------------
#CHA
# "FMC-LA04_N"
# "FMC-LA04_P"
# "FMC-LA03_N"
# "FMC-LA03_P"
# "FMC-LA02_N"
# "FMC-LA02_P"
# "FMC-LA06_N"
# "FMC-LA06_P"
# "FMC-LA05_N"
# "FMC-LA05_P"
# "FMC-LA07_N"
# "FMC-LA07_P"
# "FMC-LA08_N"
# "FMC-LA08_P"
set_property PACKAGE_PIN M21 [get_ports {cha_dat_p_i_0[0]}]
set_property PACKAGE_PIN M22 [get_ports {cha_dat_n_i_0[0]}]
set_property PACKAGE_PIN N22 [get_ports {cha_dat_p_i_0[1]}]
set_property PACKAGE_PIN P22 [get_ports {cha_dat_n_i_0[1]}]
set_property PACKAGE_PIN P17 [get_ports {cha_dat_p_i_0[2]}]
set_property PACKAGE_PIN P18 [get_ports {cha_dat_n_i_0[2]}]
set_property PACKAGE_PIN L21 [get_ports {cha_dat_p_i_0[3]}]
set_property PACKAGE_PIN L22 [get_ports {cha_dat_n_i_0[3]}]
set_property PACKAGE_PIN J18 [get_ports {cha_dat_p_i_0[4]}]
set_property PACKAGE_PIN K18 [get_ports {cha_dat_n_i_0[4]}]
set_property PACKAGE_PIN T16 [get_ports {cha_dat_p_i_0[5]}]
set_property PACKAGE_PIN T17 [get_ports {cha_dat_n_i_0[5]}]
set_property PACKAGE_PIN J21 [get_ports {cha_dat_p_i_0[6]}]
set_property PACKAGE_PIN J22 [get_ports {cha_dat_n_i_0[6]}]
set_property IOSTANDARD LVDS_25 [get_ports {cha_dat_n_i_0[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {cha_dat_p_i_0[0]}]
set_property DIFF_TERM TRUE [get_ports {cha_dat_n_i_0[0]}]
set_property DIFF_TERM TRUE [get_ports {cha_dat_p_i_0[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {cha_dat_n_i_0[1]}]
set_property IOSTANDARD LVDS_25 [get_ports {cha_dat_p_i_0[1]}]
set_property DIFF_TERM TRUE [get_ports {cha_dat_n_i_0[1]}]
set_property DIFF_TERM TRUE [get_ports {cha_dat_p_i_0[1]}]
set_property IOSTANDARD LVDS_25 [get_ports {cha_dat_n_i_0[2]}]
set_property IOSTANDARD LVDS_25 [get_ports {cha_dat_p_i_0[2]}]
set_property DIFF_TERM TRUE [get_ports {cha_dat_n_i_0[2]}]
set_property DIFF_TERM TRUE [get_ports {cha_dat_p_i_0[2]}]
set_property IOSTANDARD LVDS_25 [get_ports {cha_dat_n_i_0[3]}]
set_property IOSTANDARD LVDS_25 [get_ports {cha_dat_p_i_0[3]}]
set_property DIFF_TERM TRUE [get_ports {cha_dat_n_i_0[3]}]
set_property DIFF_TERM TRUE [get_ports {cha_dat_p_i_0[3]}]
set_property IOSTANDARD LVDS_25 [get_ports {cha_dat_n_i_0[4]}]
set_property IOSTANDARD LVDS_25 [get_ports {cha_dat_p_i_0[4]}]
set_property DIFF_TERM TRUE [get_ports {cha_dat_n_i_0[4]}]
set_property DIFF_TERM TRUE [get_ports {cha_dat_p_i_0[4]}]
set_property IOSTANDARD LVDS_25 [get_ports {cha_dat_n_i_0[5]}]
set_property IOSTANDARD LVDS_25 [get_ports {cha_dat_p_i_0[5]}]
set_property DIFF_TERM TRUE [get_ports {cha_dat_n_i_0[5]}]
set_property DIFF_TERM TRUE [get_ports {cha_dat_p_i_0[5]}]
set_property IOSTANDARD LVDS_25 [get_ports {cha_dat_n_i_0[6]}]
set_property IOSTANDARD LVDS_25 [get_ports {cha_dat_p_i_0[6]}]
set_property DIFF_TERM TRUE [get_ports {cha_dat_n_i_0[6]}]
set_property DIFF_TERM TRUE [get_ports {cha_dat_p_i_0[6]}]

#CHC
# "FMC-LA11_N"
# "FMC-LA11_P"
# "FMC-LA12_N"
# "FMC-LA12_P"
# "FMC-LA10_N"
# "FMC-LA10_P"
# "FMC-LA09_N"
# "FMC-LA09_P"
# "FMC-LA13_N"
# "FMC-LA13_P"
# "FMC-LA15_N"
# "FMC-LA15_P"
# "FMC-LA14_N"
# "FMC-LA14_P"
#set_property PACKAGE_PIN N18 [get_ports {chc_dat_n_i_0[0]}]
#set_property PACKAGE_PIN N17 [get_ports {chc_dat_p_i_0[0]}]
#set_property PACKAGE_PIN P21 [get_ports {chc_dat_n_i_0[1]}]
#set_property PACKAGE_PIN P20 [get_ports {chc_dat_p_i_0[1]}]
#set_property PACKAGE_PIN T19 [get_ports {chc_dat_n_i_0[2]}]
#set_property PACKAGE_PIN R19 [get_ports {chc_dat_p_i_0[2]}]
#set_property PACKAGE_PIN R21 [get_ports {chc_dat_n_i_0[3]}]
#set_property PACKAGE_PIN R20 [get_ports {chc_dat_p_i_0[3]}]
#set_property PACKAGE_PIN M17 [get_ports {chc_dat_n_i_0[4]}]
#set_property PACKAGE_PIN L17 [get_ports {chc_dat_p_i_0[4]}]
#set_property PACKAGE_PIN J17 [get_ports {chc_dat_n_i_0[5]}]
#set_property PACKAGE_PIN J16 [get_ports {chc_dat_p_i_0[5]}]
#set_property PACKAGE_PIN K20 [get_ports {chc_dat_n_i_0[6]}]
#set_property PACKAGE_PIN K19 [get_ports {chc_dat_p_i_0[6]}]

#set_property IOSTANDARD LVDS_25 [get_ports {chc_dat_n_i_0[0]}]
#set_property IOSTANDARD LVDS_25 [get_ports {chc_dat_p_i_0[0]}]
#set_property DIFF_TERM TRUE     [get_ports {chc_dat_n_i_0[0]}]
#set_property DIFF_TERM TRUE     [get_ports {chc_dat_p_i_0[0]}]
#set_property IOSTANDARD LVDS_25 [get_ports {chc_dat_n_i_0[1]}]
#set_property IOSTANDARD LVDS_25 [get_ports {chc_dat_p_i_0[1]}]
#set_property DIFF_TERM TRUE     [get_ports {chc_dat_n_i_0[1]}]
#set_property DIFF_TERM TRUE     [get_ports {chc_dat_p_i_0[1]}]
#set_property IOSTANDARD LVDS_25 [get_ports {chc_dat_n_i_0[2]}]
#set_property IOSTANDARD LVDS_25 [get_ports {chc_dat_p_i_0[2]}]
#set_property DIFF_TERM TRUE     [get_ports {chc_dat_n_i_0[2]}]
#set_property DIFF_TERM TRUE     [get_ports {chc_dat_p_i_0[2]}]
#set_property IOSTANDARD LVDS_25 [get_ports {chc_dat_n_i_0[3]}]
#set_property IOSTANDARD LVDS_25 [get_ports {chc_dat_p_i_0[3]}]
#set_property DIFF_TERM TRUE     [get_ports {chc_dat_n_i_0[3]}]
#set_property DIFF_TERM TRUE     [get_ports {chc_dat_p_i_0[3]}]
#set_property IOSTANDARD LVDS_25 [get_ports {chc_dat_n_i_0[4]}]
#set_property IOSTANDARD LVDS_25 [get_ports {chc_dat_p_i_0[4]}]
#set_property DIFF_TERM TRUE     [get_ports {chc_dat_n_i_0[4]}]
#set_property DIFF_TERM TRUE     [get_ports {chc_dat_p_i_0[4]}]
#set_property IOSTANDARD LVDS_25 [get_ports {chc_dat_n_i_0[5]}]
#set_property IOSTANDARD LVDS_25 [get_ports {chc_dat_p_i_0[5]}]
#set_property DIFF_TERM TRUE     [get_ports {chc_dat_n_i_0[5]}]
#set_property DIFF_TERM TRUE     [get_ports {chc_dat_p_i_0[5]}]
#set_property IOSTANDARD LVDS_25 [get_ports {chc_dat_n_i_0[6]}]
#set_property IOSTANDARD LVDS_25 [get_ports {chc_dat_p_i_0[6]}]
#set_property DIFF_TERM TRUE     [get_ports {chc_dat_n_i_0[6]}]
#set_property DIFF_TERM TRUE     [get_ports {chc_dat_p_i_0[6]}]

#CHE
# "FMC-LA23_N"
# "FMC-LA23_P"
# "FMC-LA19_N"
# "FMC-LA19_P"
# "FMC-LA20_N"
# "FMC-LA20_P"
# "FMC-LA16_N"
# "FMC-LA16_P"
# "FMC-LA22_N"
# "FMC-LA22_P"
# "FMC-LA26_N"
# "FMC-LA26_P"
# "FMC-LA21_N"
# "FMC-LA21_P"
#set_property PACKAGE_PIN D15 [get_ports {che_dat_n_i_0[0]}]
#set_property PACKAGE_PIN E15 [get_ports {che_dat_p_i_0[0]}]
#set_property PACKAGE_PIN G16 [get_ports {che_dat_n_i_0[1]}]
#set_property PACKAGE_PIN G15 [get_ports {che_dat_p_i_0[1]}]
#set_property PACKAGE_PIN G21 [get_ports {che_dat_n_i_0[2]}]
#set_property PACKAGE_PIN G20 [get_ports {che_dat_p_i_0[2]}]
#set_property PACKAGE_PIN K21 [get_ports {che_dat_n_i_0[3]}]
#set_property PACKAGE_PIN J20 [get_ports {che_dat_p_i_0[3]}]
#set_property PACKAGE_PIN F19 [get_ports {che_dat_n_i_0[4]}]
#set_property PACKAGE_PIN G19 [get_ports {che_dat_p_i_0[4]}]
#set_property PACKAGE_PIN E18 [get_ports {che_dat_n_i_0[5]}]
#set_property PACKAGE_PIN F18 [get_ports {che_dat_p_i_0[5]}]
#set_property PACKAGE_PIN E20 [get_ports {che_dat_n_i_0[6]}]
#set_property PACKAGE_PIN E19 [get_ports {che_dat_p_i_0[6]}]

#set_property IOSTANDARD LVDS_25 [get_ports {che_dat_n_i_0[0]}]
#set_property IOSTANDARD LVDS_25 [get_ports {che_dat_p_i_0[0]}]
#set_property DIFF_TERM TRUE     [get_ports {che_dat_n_i_0[0]}]
#set_property DIFF_TERM TRUE     [get_ports {che_dat_p_i_0[0]}]
#set_property IOSTANDARD LVDS_25 [get_ports {che_dat_n_i_0[1]}]
#set_property IOSTANDARD LVDS_25 [get_ports {che_dat_p_i_0[1]}]
#set_property DIFF_TERM TRUE     [get_ports {che_dat_n_i_0[1]}]
#set_property DIFF_TERM TRUE     [get_ports {che_dat_p_i_0[1]}]
#set_property IOSTANDARD LVDS_25 [get_ports {che_dat_n_i_0[2]}]
#set_property IOSTANDARD LVDS_25 [get_ports {che_dat_p_i_0[2]}]
#set_property DIFF_TERM TRUE     [get_ports {che_dat_n_i_0[2]}]
#set_property DIFF_TERM TRUE     [get_ports {che_dat_p_i_0[2]}]
#set_property IOSTANDARD LVDS_25 [get_ports {che_dat_n_i_0[3]}]
#set_property IOSTANDARD LVDS_25 [get_ports {che_dat_p_i_0[3]}]
#set_property DIFF_TERM TRUE     [get_ports {che_dat_n_i_0[3]}]
#set_property DIFF_TERM TRUE     [get_ports {che_dat_p_i_0[3]}]
#set_property IOSTANDARD LVDS_25 [get_ports {che_dat_n_i_0[4]}]
#set_property IOSTANDARD LVDS_25 [get_ports {che_dat_p_i_0[4]}]
#set_property DIFF_TERM TRUE     [get_ports {che_dat_n_i_0[4]}]
#set_property DIFF_TERM TRUE     [get_ports {che_dat_p_i_0[4]}]
#set_property IOSTANDARD LVDS_25 [get_ports {che_dat_n_i_0[5]}]
#set_property IOSTANDARD LVDS_25 [get_ports {che_dat_p_i_0[5]}]
#set_property DIFF_TERM TRUE     [get_ports {che_dat_n_i_0[5]}]
#set_property DIFF_TERM TRUE     [get_ports {che_dat_p_i_0[5]}]
#set_property IOSTANDARD LVDS_25 [get_ports {che_dat_n_i_0[6]}]
#set_property IOSTANDARD LVDS_25 [get_ports {che_dat_p_i_0[6]}]
#set_property DIFF_TERM TRUE     [get_ports {che_dat_n_i_0[6]}]
#set_property DIFF_TERM TRUE     [get_ports {che_dat_p_i_0[6]}]


#CHG
# "FMC-LA29_N"
# "FMC-LA29_P"
# "FMC-LA27_N"
# "FMC-LA27_P"
# "FMC-LA24_N"
# "FMC-LA24_P"
# "FMC-LA25_N"
# "FMC-LA25_P"
# "FMC-LA28_N"
# "FMC-LA28_P"
# "FMC-LA30_N"
# "FMC-LA30_P"
# "FMC-LA31_N"
# "FMC-LA31_P"
set_property PACKAGE_PIN C17 [get_ports {chg_dat_p_i_0[0]}]
set_property PACKAGE_PIN C18 [get_ports {chg_dat_n_i_0[0]}]
set_property PACKAGE_PIN E21 [get_ports {chg_dat_p_i_0[1]}]
set_property PACKAGE_PIN D21 [get_ports {chg_dat_n_i_0[1]}]
set_property PACKAGE_PIN A18 [get_ports {chg_dat_p_i_0[2]}]
set_property PACKAGE_PIN A19 [get_ports {chg_dat_n_i_0[2]}]
set_property PACKAGE_PIN D22 [get_ports {chg_dat_p_i_0[3]}]
set_property PACKAGE_PIN C22 [get_ports {chg_dat_n_i_0[3]}]
set_property PACKAGE_PIN A16 [get_ports {chg_dat_p_i_0[4]}]
set_property PACKAGE_PIN A17 [get_ports {chg_dat_n_i_0[4]}]
set_property PACKAGE_PIN C15 [get_ports {chg_dat_p_i_0[5]}]
set_property PACKAGE_PIN B15 [get_ports {chg_dat_n_i_0[5]}]
set_property PACKAGE_PIN B16 [get_ports {chg_dat_p_i_0[6]}]
set_property PACKAGE_PIN B17 [get_ports {chg_dat_n_i_0[6]}]

set_property IOSTANDARD LVDS_25 [get_ports {chg_dat_n_i_0[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {chg_dat_p_i_0[0]}]
set_property DIFF_TERM TRUE [get_ports {chg_dat_n_i_0[0]}]
set_property DIFF_TERM TRUE [get_ports {chg_dat_p_i_0[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {chg_dat_n_i_0[1]}]
set_property IOSTANDARD LVDS_25 [get_ports {chg_dat_p_i_0[1]}]
set_property DIFF_TERM TRUE [get_ports {chg_dat_n_i_0[1]}]
set_property DIFF_TERM TRUE [get_ports {chg_dat_p_i_0[1]}]
set_property IOSTANDARD LVDS_25 [get_ports {chg_dat_n_i_0[2]}]
set_property IOSTANDARD LVDS_25 [get_ports {chg_dat_p_i_0[2]}]
set_property DIFF_TERM TRUE [get_ports {chg_dat_n_i_0[2]}]
set_property DIFF_TERM TRUE [get_ports {chg_dat_p_i_0[2]}]
set_property IOSTANDARD LVDS_25 [get_ports {chg_dat_n_i_0[3]}]
set_property IOSTANDARD LVDS_25 [get_ports {chg_dat_p_i_0[3]}]
set_property DIFF_TERM TRUE [get_ports {chg_dat_n_i_0[3]}]
set_property DIFF_TERM TRUE [get_ports {chg_dat_p_i_0[3]}]
set_property IOSTANDARD LVDS_25 [get_ports {chg_dat_n_i_0[4]}]
set_property IOSTANDARD LVDS_25 [get_ports {chg_dat_p_i_0[4]}]
set_property DIFF_TERM TRUE [get_ports {chg_dat_n_i_0[4]}]
set_property DIFF_TERM TRUE [get_ports {chg_dat_p_i_0[4]}]
set_property IOSTANDARD LVDS_25 [get_ports {chg_dat_n_i_0[5]}]
set_property IOSTANDARD LVDS_25 [get_ports {chg_dat_p_i_0[5]}]
set_property DIFF_TERM TRUE [get_ports {chg_dat_n_i_0[5]}]
set_property DIFF_TERM TRUE [get_ports {chg_dat_p_i_0[5]}]
set_property IOSTANDARD LVDS_25 [get_ports {chg_dat_n_i_0[6]}]
set_property IOSTANDARD LVDS_25 [get_ports {chg_dat_p_i_0[6]}]
set_property DIFF_TERM TRUE [get_ports {chg_dat_n_i_0[6]}]
set_property DIFF_TERM TRUE [get_ports {chg_dat_p_i_0[6]}]





#FMC-I2C
set_property PACKAGE_PIN R7 [get_ports IIC_0_scl_io]
set_property PACKAGE_PIN U7 [get_ports IIC_0_sda_io]
set_property IOSTANDARD LVCMOS33 [get_ports IIC_0_scl_io]
set_property IOSTANDARD LVCMOS33 [get_ports IIC_0_sda_io]

#FMC-SPI
# "FMC-LA32_N"
# set_property PACKAGE_PIN A22 [get_ports SPI0_MOSI_O_0]
set_property PACKAGE_PIN A22 [get_ports SPI_0_io0_io]
# "FMC-LA32_P"
# set_property PACKAGE_PIN A21 [get_ports SPI0_SCLK_O_0]
set_property PACKAGE_PIN A21 [get_ports SPI_0_sck_io]
# "FMC-LA33_P"
# set_property PACKAGE_PIN B21 [get_ports SPI0_SS_O_0]
set_property PACKAGE_PIN B21 [get_ports SPI_0_ss_io]

set_property IOSTANDARD LVCMOS25 [get_ports SPI_0_io0_io]
set_property IOSTANDARD LVCMOS25 [get_ports SPI_0_sck_io]
set_property IOSTANDARD LVCMOS25 [get_ports SPI_0_ss_io]



##SPI to PMOD for debugging
# "JA1"
set_property PACKAGE_PIN Y11 [get_ports SPI_0_io1_io]
# "JA2"
set_property PACKAGE_PIN AA11 [get_ports SPI_0_ss1_o]
# "JA3"
set_property PACKAGE_PIN Y10 [get_ports SPI_0_ss2_o]

set_property IOSTANDARD LVCMOS33 [get_ports SPI_0_io1_io]
set_property IOSTANDARD LVCMOS33 [get_ports SPI_0_ss1_o]
set_property IOSTANDARD LVCMOS33 [get_ports SPI_0_ss2_o]










#set_property PACKAGE_PIN B22 [get_ports {spi_rtl_io1_io}];
#set_property IOSTANDARD LVCMOS25 [get_ports {spi_rtl_io1_io}];

#SPI_0_0_io0_io : inout STD_LOGIC;
#SPI_0_0_io1_io : inout STD_LOGIC;
#SPI_0_0_sck_io : inout STD_LOGIC;
#SPI_0_0_ss1_o : out STD_LOGIC;
#SPI_0_0_ss2_o : out STD_LOGIC;
#SPI_0_0_ss_io : inout STD_LOGIC;


#set_property PACKAGE_PIN C19 [get_ports {FMC_CLK1_N}];  # "FMC-CLK1_N"
#set_property PACKAGE_PIN D18 [get_ports {FMC_CLK1_P}];  # "FMC-CLK1_P"


#set_property PACKAGE_PIN A22 [get_ports {FMC_LA32_N}];  # "FMC-LA32_N"
#set_property PACKAGE_PIN A21 [get_ports {FMC_LA32_P}];  # "FMC-LA32_P"
#set_property PACKAGE_PIN B22 [get_ports {FMC_LA33_N}];  # "FMC-LA33_N"
#set_property PACKAGE_PIN B21 [get_ports {FMC_LA33_P}];  # "FMC-LA33_P"



